<?php

require_once __DIR__ . '/../app/autoload.php';
require_once __DIR__ . '/AppKernel.php';

$kernel = new AppKernel('test', false);
$kernel->boot();