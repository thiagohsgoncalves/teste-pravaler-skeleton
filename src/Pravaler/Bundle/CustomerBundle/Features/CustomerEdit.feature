Feature: Customer Edit links
  In order to help users know which customer they can edit, we need to display the edit link

  Background:
    Given I have a clean database
    And I have the following customer
      | _id                       | name               | email             | phone        |
      | 565745744fc2c226728b4565  | Zangief | zangief@streetfighter.com | +5511952811000 |

  Scenario: Success edit a customer
    Given I am on the customer edit page
    Then I should see "Save"
    Then I fill in "customer_edit[name]" with "Blanka"
    Then I fill in "customer_edit[email]" with "blanka@streetfighter.com"
    Then I fill in "customer_edit[phone]" with "+5511952812000"
    And I press "Save"
    Then I should see "Success"
