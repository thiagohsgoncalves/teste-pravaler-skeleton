<?php

namespace Pravaler\Bundle\DriverBundle\Tests\Form;

use Pravaler\Bundle\BaseBundle\Service\PaginableResult;

class PaginableResultTest extends \PHPUnit_Framework_TestCase
{
    public function testPaginableResultMustHaveCorrectValuesForPagination()
    {
        $paginableResult = new PaginableResult(23, 1);
        $paginableResult->setItems(['any' => 'array']);
        $this->assertEquals(2, $paginableResult->getLastPage());
        $this->assertEquals(1, $paginableResult->getCurrentPage());
        $this->assertEquals(10, $paginableResult->getItemsPerPage());
        $this->assertEquals(10, $paginableResult->getOffsetForPage());
        $this->assertEquals(2, $paginableResult->getNextPage());
        $this->assertEquals(23, $paginableResult->getTotal());
        $this->assertEquals(0, $paginableResult->getPreviousPage());
        $this->assertEquals(['any' => 'array'], $paginableResult->getItems());
    }
}
