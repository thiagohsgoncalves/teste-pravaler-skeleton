<?php

namespace Pravaler\Bundle\BaseBundle\Service;

class PaginableResult
{
    private $items;
    private $total;
    private $itemsPerPage = 10;
    private $currentPage;

    public function __construct($total, $currentPage)
    {
        $this->total = $total;
        $this->currentPage = $currentPage;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    public function getOffsetForPage()
    {
        return $this->getCurrentPage() * $this->getItemsPerPage();
    }

    public function getLastPage()
    {
        return ceil($this->getTotal()/$this->getItemsPerPage()) - 1;
    }

    public function getPreviousPage()
    {
        if ($this->getCurrentPage() <= 0) {
            return false;
        }

        return $this->getCurrentPage() - 1;
    }

    public function getNextPage()
    {
        if (($this->getCurrentPage()) >= $this->getLastPage()) {
            return false;
        }

        return $this->getCurrentPage() + 1;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function setItems($items)
    {
        $this->items = $items;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }
}
