<?php

namespace Pravaler\Component\Customer\Exception;

class CustomerNotFoundException extends \Exception
{
    public function __construct(
        $message = 'Customer not found',
        $code = 400,
        \Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
