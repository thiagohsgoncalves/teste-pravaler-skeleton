<?php

namespace Pravaler\Component\Customer\Model;

class CustomerModel
{
    private $customerRequest;

    public function __construct(CustomerRequest $customerRequest)
    {
        $this->customerRequest = $customerRequest;
    }

    public function updateCustomer(Customer $customer)
    {
        $customerRequestData = [
            'name' => $customer->getName(),
            'email' => $customer->getEmail(),
            'phone' => $customer->getPhone(),
            'is_active' => (int) $customer->getIsActive()
        ];
        $this->customerRequest->sendPatchRequest($customerRequestData, $customer->getId());
    }
}
