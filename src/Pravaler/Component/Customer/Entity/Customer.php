<?php
namespace Pravaler\Component\Customer\Entity;

use JMS\Serializer\Annotation as JMS;

class Customer
{
    /**
     * @JMS\SerializedName("_id")
     * @JMS\Type("MongoId")
     */
    private $id;

    /**
     * @JMS\Type("string")
     */
    private $name;

    /**
     * @JMS\Type("string")
     */
    private $email;

    /**
     * @JMS\Type("ArrayCollection<Pravaler\Component\Driver\Entity\Device>")
     */
    private $devices;

    /**
     * @JMS\Type("string")
     */
    private $areaCode;

    /**
     * @JMS\Type("string")
     */
    private $phone;

    /**
     * @JMS\Type("MongoDate")
     */
    private $acceptedConditionsAt;

    /**
     * @JMS\Type("MongoDate")
     */
    private $lastLoginAt;

    /**
     * @JMS\SerializedName("is_active")
     * @JMS\Type("boolean")
     */
    private $isActive;

    /**
     * @JMS\SerializedName("created_at")
     * @JMS\Type("MongoDate")
     */
    private $createdAt;

    /**
     * @JMS\SerializedName("updated_at")
     * @JMS\Type("MongoDate")
     */
    private $updatedAt;

    /**
     * @JMS\Type("string")
     */
    private $locale;

    /**
     * @JMS\Type("string")
     */
    private $city;

    /**
     * @JMS\Type("string")
     */
    private $state;

    /**
     * @JMS\Type("string")
     */
    private $country;

    /**
     * @JMS\SerializedName("is_corporate")
     * @JMS\Type("boolean")
     */
    private $isCorporate;

    /**
     * @JMS\SerializedName("corp")
     * @JMS\Type("Pravaler\Component\Customer\Entity\Corporate")
     */
    private $corporate;

    /**
     * @JMS\Type("boolean")
     * @JMS\SerializedName("is_malicious")
     */
    private $isMalicious;

    /**
     * @JMS\Type("string")
     */
    private $signUpPhone;

    /**
     * @JMS\SerializedName("fraud_status")
     * @JMS\Type("string")
     */
    private $fraudStatus;

    /**
     * @JMS\SerializedName("fraud_reason")
     * @JMS\Type("string")
     */
    private $fraudReason;

    /**
     * @JMS\SerializedName("fraud_status_date")
     * @JMS\Type("MongoDate")
     */
    private $fraudStatusDate;

    /**
     * @JMS\SerializedName("photo_url")
     * @JMS\Type("string")
     */
    private $photoUrl;

    /**
     * @JMS\SerializedName("is_multiple_rides")
     * @JMS\Type("boolean")
     */
    private $isMultipleRides;

    /**
     * @JMS\SerializedName("brand")
     * @JMS\Type("string")
     */
    private $brand;

    /**
     * @JMS\SerializedName("previous_brand_id")
     * @JMS\Type("string")
     */
    private $previousBrandId;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getDevices()
    {
        return $this->devices;
    }

    public function setDevices($devices)
    {
        $this->devices = $devices;
    }

    public function getAreaCode()
    {
        return $this->areaCode;
    }

    public function setAreaCode($areaCode)
    {
        $this->areaCode = $areaCode;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getAcceptedConditionsAt()
    {
        return $this->acceptedConditionsAt;
    }

    public function setAcceptedConditionsAt($acceptedConditionsAt)
    {
        $this->acceptedConditionsAt = $acceptedConditionsAt;
    }

    public function getLastLoginAt()
    {
        return $this->lastLoginAt;
    }

    public function setLastLoginAt($lastLoginAt)
    {
        $this->lastLoginAt = $lastLoginAt;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getIsCorporate()
    {
        return $this->isCorporate;
    }

    public function setIsCorporate($isCorporate)
    {
        $this->isCorporate = $isCorporate;
    }

    public function getCorporate()
    {
        return $this->corporate;
    }

    public function setCorporate($corporate)
    {
        $this->corporate = $corporate;
    }

    public function getIsMalicious()
    {
        return $this->isMalicious;
    }

    public function setIsMalicious($isMalicious)
    {
        $this->isMalicious = $isMalicious;
    }

    public function getSignUpPhone()
    {
        return $this->signUpPhone;
    }

    public function setSignUpPhone($signUpPhone)
    {
        $this->signUpPhone = $signUpPhone;
    }

    public function getFraudStatus()
    {
        return $this->fraudStatus;
    }

    public function setFraudStatus($fraudStatus)
    {
        $this->fraudStatus = $fraudStatus;
    }

    public function getFraudReason()
    {
        return $this->fraudReason;
    }

    public function setFraudReason($fraudReason)
    {
        $this->fraudReason = $fraudReason;
    }

    public function getFraudStatusDate()
    {
        return $this->fraudStatusDate;
    }

    public function setFraudStatusDate($fraudStatusDate)
    {
        $this->fraudStatusDate = $fraudStatusDate;
    }

    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;
    }

    public function getIsMultipleRides()
    {
        return $this->isMultipleRides;
    }

    public function setIsMultipleRides($isMultipleRides)
    {
        $this->isMultipleRides = $isMultipleRides;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    public function getPreviousBrandId()
    {
        return $this->previousBrandId;
    }

    public function setPreviousBrandId($previousBrandId)
    {
        $this->previousBrandId = $previousBrandId;
    }
}
