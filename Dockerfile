FROM php:5.6.32-fpm-alpine

RUN set -xe \
	&& apk add --no-cache --virtual .build-deps \
		$PHPIZE_DEPS \
		cyrus-sasl-dev \
		icu-dev \
		libpng-dev \
		openssl-dev \
	&& apk add --no-cache --virtual .run-deps \
		git \
		icu-libs \
		libpng \
		libsasl \
		openssl \
	&& docker-php-ext-install -j "$(getconf _NPROCESSORS_ONLN)" \
		gd \
		intl \
		pdo_mysql \
	&& pecl install \
		memcache-2.2.7 \
		mongo-1.6.16 \
		stomp-1.0.9 \
	&& docker-php-ext-enable \
		memcache \
		mongo \
		stomp \
	&& apk del .build-deps \
	&& rm -r /tmp/pear

RUN set -xe \
	&& mkdir /opt \
	&& curl -sS https://download.newrelic.com/php_agent/archive/7.6.0.201/newrelic-php5-7.6.0.201-linux-musl.tar.gz | tar xzC /opt \
	&& NR_INSTALL_SILENT=1 /opt/newrelic-php5-7.6.0.201-linux-musl/newrelic-install install \
	&& sed -i \
		-e 's/;\?newrelic\.enabled =.*/newrelic.enabled = ${NEWRELIC_ENABLED}/' \
		-e 's/;\?newrelic\.license =.*/newrelic.license = ${NEWRELIC_LICENSE}/' \
		-e 's/;\?newrelic\.appname =.*/newrelic.appname = ${NEWRELIC_APPNAME}/' \
		/usr/local/etc/php/conf.d/newrelic.ini \
	&& rm -r /tmp/nrinstall*

ENV NEWRELIC_APPNAME Pravaler
ENV NEWRELIC_ENABLED false
ENV app_logs_path /var/log/app

COPY dockerconfigs/php.ini /usr/local/etc/php/
COPY dockerconfigs/pravaler.ini /usr/local/etc/php/conf.d/
COPY dockerconfigs/zzz-pravaler.conf /usr/local/etc/php-fpm.d/

COPY composer.json composer.lock /tmp/

RUN set -xe \
	&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
	&& composer install \
		--no-dev \
		--no-interaction \
		--no-progress \
		--prefer-dist \
		--no-autoloader \
		--no-plugins \
		--no-scripts \
		--working-dir=/tmp \
	&& rm -r /tmp/composer.json /tmp/composer.lock /tmp/vendor

COPY . /var/www/pravaler

RUN set -xe \
	&& install -d -m 755 \
		/var/log/php-fpm \
		/var/run/php-fpm \
	&& install -d -m 755 -o www-data -g www-data \
		/var/www/pravaler/var/cache \
		/var/www/pravaler/var/logs \
		"$app_logs_path" \
	&& cp /var/www/pravaler/app/config/.env.dist /var/www/pravaler/app/config/.env \
	&& APP_BUILD=1 SYMFONY__ENV=prod SYMFONY_ENV=prod composer install \
		--no-dev \
		--no-interaction \
		--no-progress \
		--prefer-dist \
		--optimize-autoloader \
		--working-dir=/var/www/pravaler \
	&& rm -r \
		/var/www/pravaler/var/cache/prod \
		~/.composer

WORKDIR /var/www/pravaler
RUN rmdir /var/www/html
